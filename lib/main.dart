import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _animationController;

  Animation<double> _animation;

  // double height = 100;
  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
      reverseDuration: Duration(seconds: 1),
    );
    _animation =
        Tween<double>(begin: 100, end: 300).animate(_animationController);

    _animationController.addListener(update);

    super.initState();
  }

  void update() => setState(() {});

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, _animation.value),
        child: AnimatedContainer(
          height: _animation.value,
          width: double.infinity,
          duration: Duration(seconds: 1),
          child: AppBar(
            toolbarHeight: 100,
            title: Text(widget.title),
          ),
        ),
      ),
      body: Center(child: Column(
        children: [
          FlatButton(
            child: Text('tap me'),
            onPressed: (){_animationController.forward();},
          ),
          FlatButton(
            child: Text('reverse'),
            onPressed: (){_animationController.reset();},
          ),
        ],
      ),),
     /* floatingActionButton: FloatingActionButton(
        onPressed: (){_animationController.forward();},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),*/
    );
  }
}
